package edu.binghamton.cs.hello;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

public class ProgressWheel extends Sprite {
    final static int DIAMETER = 150;

    private float total, sweepAngle;

    ProgressWheel(Context context) {
        super(context);
        this.total = 1.0f;
        this.sweepAngle = 0;
        this.paint.setColor(Color.GREEN);
        this.paint.setStyle(Paint.Style.STROKE);
        this.paint.setStrokeWidth(20);
        this.paint.setStrokeCap(Paint.Cap.ROUND);
    }

    ProgressWheel(Context context, int total) {
        this(context);
        this.total = total;
    }

    @Override
    public void draw(Canvas canvas) {
        RectF oval = new RectF(this.x - DIAMETER/2, this.y - DIAMETER/2,
                this.x + DIAMETER/2, this.y + DIAMETER/2);
        canvas.drawArc(oval, 270, sweepAngle, false, this.paint);
    }

    public void update(int progress) {
        this.sweepAngle = 360 * (progress / this.total);
    }
}
